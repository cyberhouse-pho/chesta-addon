# Statamic Chesta - Custom Content Provider
[![Statamic 3.0+](https://img.shields.io/badge/Statamic-3.0+-FF269E?style=for-the-badge&link=https://statamic.com)](https://statamic.com)
[![Latest Version on Packagist](https://img.shields.io/packagist/v/jonassiewertsen/statamic-oh-dear.svg?style=for-the-badge)](https://packagist.org/packages/netgardeners/chesta)

## Chesta
This addon will make it easy to add a custom content provider for arbitrary collections.

## Installation
### Step 1
Pull in your package with composer
```bash
composer require netgardeners/chesta
```

### Step 2
Create a custom child store by extending the `Cyberhouse\Chesta\AbstractEntriesStore` class.

### Step 3
Publish the chesta config ...
```bash
php artisan vendor:publish --provider="Cyberhouse\Chesta\ServiceProvider" --force
```
... and add a mapping for the collection and the custom child store.
