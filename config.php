<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Child Stores
    |--------------------------------------------------------------------------
    |
    | Here you may map collections to custom child stores which act as
    | content providers. Any entry of this collection will then be pulled
    | from the defined child store.
    |
    */

    'childStores' => [

        //'examples' => \App\Stores\EmployeeEntriesStore::class,

    ],

];
