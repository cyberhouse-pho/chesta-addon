<?php

namespace Cyberhouse\Chesta;

use Illuminate\Support\Collection;
use Statamic\Entries\Entry;

class ExampleEntriesStore extends AbstractEntriesStore
{
    /**
     * By connection the example entry store to a collection,
     * fetching entries will return the data from this function.
     * A corresponding blueprint for this collection has to exist.
     *
     * chesta.php
     * 'childStores' => [
     *   'examples' => \Cyberhouse\Chesta\ExampleEntriesStore::class,
     * ],
     *
     * template.antlers.html
     * {{ collection:examples limit="20" }}
     *   {{ title }}<br/>
     * {{ /collection:examples }}
     *
     * @param Collection $entries
     */
    protected function getEntries(Collection &$entries)
    {
        $entry = new Entry();
        $entry->id(1);
        $entry->set('title', 'Title 1');
        $entries->put($entry->id(), $entry);

        $entry = new Entry();
        $entry->id(2);
        $entry->set('title', 'Title 2');
        $entries->put($entry->id(), $entry);

        $entry = new Entry();
        $entry->id(3);
        $entry->set('title', 'Title 3');
        $entries->put($entry->id(), $entry);
    }
}
