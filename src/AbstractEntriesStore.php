<?php

namespace Cyberhouse\Chesta;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Statamic\Entries\Entry;
use Statamic\Stache\Stores\CollectionEntriesStore;

abstract class AbstractEntriesStore extends CollectionEntriesStore
{
    /**
     * will get called frequently -> consider caching.
     * fields are used for indexing.
     * simple entries with just the id would be sufficient to return thou.
     *
     * @param Collection $entries
     */
    abstract protected function getEntries(Collection &$entries);

    /**
     * may be implemented to be used for speedup
     *
     * @return int
     */
    protected function getCount() : int {
        return Cache::remember('chesta::count', now()->addMinutes(5), function() {
            $collection = new Collection();
            $this->getEntries($collection);
            return $collection->count();
        });
    }

    /**
     * full entry item with all fields
     *
     * @param string|int $key
     * @param Entry $entry
     */
    protected function getEntry($key, Entry &$entry) {
        $entry = $this->getItems(null)->get($key);
    }

    /**
     * provide the keys of the changed entries to invalidate their cache entry
     *
     * @param $lastCheckTimestamp
     * @param Collection $keys
     * @return boolean
     */
    public function getChangedEntryKeys(Collection &$keys, $lastCheckTimestamp) {
        return false;
    }

    /**
     * provide info if given entry may have changed to invalidate cachen entry
     * and refetch it
     *
     * @param Entry $entry
     * @return boolean
     */
    public function hasEntryChanged(Entry $entry) : bool {
        return false;
    }

    public function handleFileChanges()
    {
        if ($this->fileChangesHandled) {
            return;
        }

        $keys = new Collection();
        $nextCheckTimestamp = now();
        $lastCheckTimestamp = Cache::get('chesta::lastamp', 0);
        if (($this->fileChangesHandled = $this->getChangedEntryKeys($keys, $lastCheckTimestamp)) !== false) {
            // check for changed + new
            Cache::put('chesta::lastamp', $nextCheckTimestamp);
            foreach ($keys as $key) {
                Cache::forget($this->getItemCacheKey($key));
            }
        }
    }

    // -- INTERNAL IMPLEMENTATIONS --

    public function index($name)
    {
        $index = parent::index($name);
        $counts = [ $index->items()->count(), $this->fetchEntries()->count(), count($this->paths()), $this->getCount() ];
        if (count($this->paths()) != ($count = $this->getCount())) {}
        if (count(array_unique($counts)) !== 1) {
            Cache::forget('chesta::count');
            Cache::forget('chesta::entries');
            Cache::forget($this->pathsCacheKey());
            $index = $index->update();
        }
        return $index;
    }

    public function getItemsFromFiles()
    {
        return $this->fileItems ?: $this->fileItems = $this->fetchEntries();
    }

    public function getItems($keys)
    {
        return $this->fetchEntries();
    }

    public function getItem($key)
    {
        $entry = Cache::remember($this->getItemCacheKey($key), now()->addHours(3), function() use ($key) {
            $entry = new Entry();
            $entry->id($key);
            $entry->collection($this->childKey());
            $this->getEntry($key, $entry);
            return $entry;
        });
        if (!$this->fileChangesHandled && $this->hasEntryChanged($entry)) {
            Cache::forget($this->getItemCacheKey($key));
            $entry = $this->getEntry($key);
        }
        return $entry;
    }

    public function paths()
    {
        return $this->paths ?: $this->paths = Cache::rememberForever($this->pathsCacheKey(), function() {
            return collect(array_flip(range(1, $this->getCount())));
        });
    }

    private function fetchEntries() {
        return Cache::rememberForever('chesta::entries', function() {
            $collection = new Collection();
            $this->getEntries($collection);
            $collection->each(function($entry) {
                $entry->slug($entry->slug() ?: $entry->id());
                $entry->collection($this->childKey());
            });
            return $collection;
        });
    }
}
