<?php

namespace Cyberhouse\Chesta;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Statamic\Stache\Stores\EntriesStore;

class CustomStore extends EntriesStore
{
    protected function createChildStore($key)
    {
        $this->childStoreCreator = function() use ($key) {
            return app(config('chesta.childStores', [])[$key] ?? $this->childStore);
        };

        $childStore = parent::createChildStore($key);
        $current = Cache::get("chesta:childStoreClass$key");
        if ($current != get_class($childStore)) {
            if ($current) Artisan::call('cache:clear');
            Cache::forever("chesta:childStoreClass$key", get_class($childStore));
        }
        return $childStore;
    }
}
