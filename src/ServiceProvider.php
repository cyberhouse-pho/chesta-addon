<?php

namespace Cyberhouse\Chesta;

use Statamic\Providers\AddonServiceProvider;

class ServiceProvider extends AddonServiceProvider
{
    public function register()
    {
        config(array_replace_recursive(
            config()->all(),
            [ 'statamic' => [ 'stache' =>  [ 'stores' =>  ['entries' => [
                'class' => CustomStore::class
            ]]]]]
        ));
    }

    public function boot()
    {
        parent::boot();

        if ($this->app->runningInConsole()) {
            // Config
            $this->publishes([
                __DIR__ . '/../config.php' => config_path('chesta.php'),
            ], 'chesta-config');
        }
    }
}
